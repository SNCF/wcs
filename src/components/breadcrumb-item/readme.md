# wcs-breadcrumb-item



<!-- Auto Generated Below -->


## Overview

The breadcrumb item represents a link inside a breadcrumb.

## Slots

| Slot                              | Description |
| --------------------------------- | ----------- |
| `"<no-name> Main container slot"` |             |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
