# Galactic Bar

<!-- Auto Generated Below -->


## Overview

*Part of communication design system*
This component replaces the vertical navbar for sites and applications that use the communication design system

## Properties

| Property | Attribute | Description                | Type     | Default     |
| -------- | --------- | -------------------------- | -------- | ----------- |
| `text`   | `text`    | Text to display in the bar | `string` | `undefined` |


## Slots

| Slot                              | Description |
| --------------------------------- | ----------- |
| `"<no-name> Main container slot"` |             |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
