# com-nav-item



<!-- Auto Generated Below -->


## Overview

The com-nav-item is a subcomponent of `wcs-com-nav`. It represents a list-item wrapper around a link.

## Events

| Event                   | Description                                                                                                                                                  | Type                |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------- |
| `wcsClickOnFinalAction` | Emitted when a user click on a final navigation action.  Used by the com-nav component to close the mobile menu overlay when a user click on a final action. | `CustomEvent<void>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
