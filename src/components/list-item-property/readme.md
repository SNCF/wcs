# wcs-list-item-property



<!-- Auto Generated Below -->


## Overview

The list-item-property is a subcomponent of `wcs-list-item`.
Wrapped in a `wcs-list-item-properties`, it represents a property to describe an item.

## Slots

| Slot          | Description         |
| ------------- | ------------------- |
| `"<no-name>"` | Main container slot |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
