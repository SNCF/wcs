# Breaking changes for next-release

Make sure these changes are made before releasing the next release of WCS.

## v8

- [ ] Add token `semantic.color.border.focus.base.on-primary`
- [ ] Add token `semantic.color.border.focus.alt.on-primary`
- [ ] Rename token `semantic.color.border.focus.base-on-secondary` to `semantic.color.border.focus.base.on-secondary` (make a group named `base`)
- [ ] Rename token `semantic.color.border.focus.alt-on-secondary` to `semantic.color.border.focus.alt.on-secondary` (make a group named `alt`)
