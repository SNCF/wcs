import {Meta, Unstyled} from '@storybook/addon-docs';

<Meta title="Documentation/Migrations/v6 to v7"/>

# Migrate from v6 to v7

WCS v7 contains breaking changes only in the following packages:
- wcs-core
- wcs-angular
- wcs-react

Version 7 introduces a comprehensive redesign of the library, incorporating a new concept: design tokens. To learn more about design tokens and their benefits, we recommend consulting [this documentation](.?path=/docs/documentation-theming--documentation#how-it-works).

To migrate from the current system to the new one, simply follow the steps :
- for **Angular** users, follow the [Angular guide](.?path=/docs/documentation-getting-started-angular--documentation#6-add-a-theme)
- for **React** users, follow the [React guide](.?path=/docs/documentation-getting-started-react--documentation#5-add-a-theme)


**Important Note for CSS Variable Overrides**

If your project uses CSS variable overrides, it is crucial to read this documentation thoroughly. Significant breaking changes have been introduced in this area, and understanding these changes is essential for a smooth transition.

## wcs-core

### wcs-tooltip

We rename default theme from `wcs` to `dark`. If you override `theme` property on `wcs-tooltip`, you should update it to `dark`.

### Fonts

Some changes have been made to the font weights. You must update your global stylesheet accordingly.

1. **`Avenir Heavy`** has been added to the available fonts. It corresponds to the **font-weight `800`** of the Avenir font family.  
You must add it to your font-face declaration if you want to use it.

```css
@font-face {
  font-family: Avenir;
  src: url("assets/fonts/avenir-heavy.woff");
  font-weight: 800;
}
```

2. **`Avenir Roman`** has been added to the available fonts. It corresponds to the **font-weight `400`** of the Avenir font family.  
You must add it to your font-face declaration if you want to use it.

```css
@font-face {
  font-family: Avenir;
  src: url("assets/fonts/avenir-roman.woff");
  font-weight: 400;
}
```

3. **`Avenir Light`** is now **font-weight `100`** instead of `300`. **`Avenir Book`** is now **font-weight `300`** instead of `400`.

To sum up :


<table>
    <thead>
        <tr>
            <th>Before</th>
            <th>After</th>
        </tr>
    </thead>
    <tbody style={{verticalAlign: 'baseline', fontFamily: 'Avenir'}}>
        
        <tr>
            <td>
                <Unstyled>
                    <span style={{fontWeight: 900}}>Avenir Black 900</span><br/>
                    <span style={{fontWeight: 500}}>Avenir Medium 500</span><br/>
                    <span style={{fontWeight: 300}}>Avenir Book 400</span><br/>
                    <span style={{fontWeight: 100}}>Avenir Light 300</span><br/>
                </Unstyled>
            </td>
            <td>
                <Unstyled>
                    <span style={{fontWeight: 900}}>Avenir Black 900</span><br/>
                    <span style={{fontWeight: 800}}>Avenir Heavy 800</span><br/>
                    <span style={{fontWeight: 500}}>Avenir Medium 500</span><br/>
                    <span style={{fontWeight: 400}}>Avenir Roman 400</span><br/>
                    <span style={{fontWeight: 300}}>Avenir Book 300</span><br/>
                    <span style={{fontWeight: 100}}>Avenir Light 100</span><br/>
                </Unstyled>
            </td>
        </tr>
    </tbody>
</table>

Please read the new [Angular](.?path=/docs/documentation-getting-started-angular--documentation) / [React](.?path=/docs/documentation-getting-started-react--documentation) documentation for further information.

### CSS Variables

> During Design Tokens migration, we have **reviewed and updated certain CSS variables names on several components**.  
> - If you have used **any of the following CSS variables below**, you need to **update them** in your project or use the new ones.  
> - If these variable doesn't appear in your codebase, you don't need to update anything.
> - You can find a full list of the new tokens in the [design tokens section](.?path=/docs/documentation-design-tokens-sncf-holding--documentation).

#### 1️⃣ Globals

##### Colors

<table>
    <thead>
    <tr>
        <th>Deleted variable</th>
        <th>Replace with</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            **`--wcs-blue`**
            **`--wcs-primary`**
            **`--wcs-base`**
        </td>
        <td>
            **`--wcs-primitive-color-primary` is used as a base, but prefer using any of these :**
            - `--wcs-semantic-color-background-action-primary-default` for an interactive background
            - `--wcs-semantic-color-background-surface-brand` for a brand, non interactive background
            - `--wcs-semantic-color-foreground-brand` for a brand, non interactive foreground (text, icon layer on top)
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-gray-dark`<br/>
            `--wcs-gray-darker`<br/>
            `--wcs-black`<br/>
            `--wcs-purple`<br/>
            `--wcs-pink`<br/>
            `--wcs-orange`<br/>
            `--wcs-teal`<br/>
            `--wcs-cyan`<br/>
        </td>
        <td>
            No specific replacement. Use the correct design token according to your needs.
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-white`
        </td>
        <td>
            - `--wcs-semantic-color-text-inverse` for white text color over a dark background (note that the token no longer contains the color value, only the usage context. Can be useful for theming).  
            - `--wcs-semantic-color-background-surface-primary` for white background color.  
            - `--wcs-semantic-color-foreground-action-primary` for white foreground color on actions.  
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-contrast`
        </td>
        <td>
            - `--wcs-semantic-color-text-inverse`
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-light`
        </td>
        <td>
            - `--wcs-semantic-color-background-surface-secondary` for light background color.  
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-gray-light`
        </td>
        <td>
            - `--wcs-semantic-color-text-secondary` for light text color.  
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-gray`
        </td>
        <td>
            - `--wcs-semantic-color-text-primary` for text color.  
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-text-light`
        </td>
        <td>
            - `--wcs-semantic-color-border-surface` for surface border color.  
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-text-disabled`
        </td>
        <td>
            - `--wcs-semantic-color-text-disabled` for disabled text.
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-text-medium`
        </td>
        <td>
            - `--wcs-semantic-color-text-tertiary` for tertiary (light gray) texts.
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-red`
        </td>
        <td>
            - `--wcs-semantic-color-text-critical` for representing a red text with error or critical actions.
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-yellow`
        </td>
        <td>
            - `--wcs-semantic-color-background-warning-default` for representing warnings.
        </td>
    </tr>
    <tr>
        <td>
            `--wcs-green`
        </td>
        <td>
            - `--wcs-semantic-color-background-success-default` for representing success states.
        </td>
    </tr>
    </tbody>
</table>

##### Font-weight

<table>
    <thead>
    <tr>
        <th>Deleted variable</th>
        <th>Replace with</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>`--wcs-font-weight-form-label`</td>
        <td>`--wcs-semantic-font-weight-medium`</td>
    </tr>
    </tbody>
</table>

##### Size

<table>
    <thead>
    <tr>
        <th>Deleted variable</th>
        <th>Replace with</th>
    </tr>
    </thead>
    <tbody style={{verticalAlign: 'baseline', fontFamily: 'Avenir'}}>
        <tr>
            <td>`--wcs-base-size`</td>
            <td>`--wcs-semantic-size-base`</td>
        </tr>
    <tr>
        <td>`--wcs-size-xl`</td>
        <td>`--wcs-semantic-size-xl`</td>
    </tr>
    <tr>
        <td>`--wcs-size-l`</td>
        <td>`--wcs-semantic-size-l`</td>
    </tr>
    <tr>
        <td>`--wcs-size-m`</td>
        <td>`--wcs-semantic-size-m`</td>
    </tr>
    <tr>
        <td>`--wcs-size-s`</td>
        <td>`--wcs-semantic-size-s`</td>
    </tr>
    <tr>
        <td>`--wcs-size-xs`</td>
        <td>`--wcs-semantic-size-xs`</td>
    </tr>
    </tbody>
</table>

> 👉 Please read the [Template guidelines](.?path=/docs/documentation-template--documentation) and update your `app-root` stylesheet according to the above changes.  
> You may for instance update these CSS lines (search and replace in your code) :  
> ```css
> height: calc(100vh - 64px); /* BEFORE */
> height: calc(100vh - 8 * var(--wcs-semantic-size-base)); /* AFTER */
> ...
> height: calc(100vh - 64px - 54px); /* BEFORE */
> height: calc(100vh - 8 * var(--wcs-semantic-size-base) - 6 * var(--wcs-semantic-size-base)); /* AFTER */
> ```

##### Border

<table>
    <thead>
    <tr>
        <th>Deleted variable</th>
        <th>Replace with</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>`--wcs-border-size`</td>
        <td>`--wcs-semantic-border-width-large`</td>
    </tr>
    <tr>
        <td>`--wcs-border-radius`</td>
        <td>`--wcs-semantic-border-radius-base`</td>
    </tr>
    </tbody>
</table>

##### Padding

> Paddings and margins variables are now merged into "spacing".

<table>
    <thead>
    <tr>
        <th>Deleted variable</th>
        <th>Replace with</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>`--wcs-padding`</td>
        <td>`--wcs-semantic-spacing-large` (16px)</td>
    </tr>
    <tr>
        <td>`--wcs-padding-l`</td>
        <td>Use a CSS calculation function : `calc(1.5 * var(--wcs-semantic-spacing-base)` (12px)</td>
    </tr>
    <tr>
        <td>`--wcs-padding-m`</td>
        <td>`--wcs-semantic-spacing-base` (8px)</td>
    </tr>
    <tr>
        <td>`--wcs-padding-s`</td>
        <td>`--wcs-semantic-spacing-small` (4px)</td>
    </tr>
    <tr>
        <td>`--wcs-text-padding`</td>
        <td>Use the correct padding design token according to your needs. We recommend using `--wcs-semantic-spacing-base`</td>
    </tr>
    </tbody>
</table>

##### Margin

> Paddings and margins variables are now merged into "spacing".

<table>
    <thead>
    <tr>
        <th>Deleted variable</th>
        <th>Replace with</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>`--wcs-base-margin`</td>
        <td>`--wcs-semantic-spacing-base` (8px)</td>
    </tr>
    <tr>
        <td>`--wcs-margin`</td>
        <td>`--wcs-semantic-spacing-large` (16px)</td>
    </tr>
    <tr>
        <td>`--wcs-margin-l`</td>
        <td>Use a CSS calculation function : `calc(1.5 * var(--wcs-semantic-spacing-base)` (12px)</td>
    </tr>
    <tr>
        <td>`--wcs-margin-m`</td>
        <td>`--wcs-semantic-spacing-base` (8px)</td>
    </tr>
    <tr>
        <td>`--wcs-margin-s`</td>
        <td>`--wcs-semantic-spacing-small` (4px)</td>
    </tr>
    </tbody>
</table>

#### 2️⃣ Specific to components

#### wcs-button

<table>
    <thead>
        <tr>
            <th>Deleted variable</th>
            <th>Replace with</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>`--wcs-button-height`</td>
            <td>
                - `--wcs-button-height-s`
                - `--wcs-button-height-m`
                - `--wcs-button-height-l`
            </td>
        </tr>
    </tbody>
</table>

#### wcs-card

- Default card mode is now `flat` instead of `raised`. Update your cards if needed.

#### wcs-input

<table>
    <thead>
        <tr>
            <th>Deleted variable</th>
            <th>Replace with</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>`--wcs-input-icon-color`</td>
            <td>
                <ul>
                    <li>`--wcs-input-icon-color-default`: default icon color</li>
                    <li>`--wcs-input-icon-color-focus`: icon color on focus</li>
                    <li>`--wcs-input-icon-color-disabled`: icon color when disabled</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

#### wcs-textarea

<table>
    <thead>
        <tr>
            <th>Deleted variable</th>
            <th>Replace with</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>`--wcs-textarea-icon-color`</td>
            <td>
                <ul>
                    <li>`--wcs-textarea-icon-color-default`: default icon color</li>
                    <li>`--wcs-textarea-icon-color-focus`: icon color on focus</li>
                    <li>`--wcs-textarea-icon-color-disabled`: icon color when disabled</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

#### wcs-select

<table>
    <thead>
        <tr>
            <th>Deleted variable</th>
            <th>Replace with</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>`--wcs-select-icon-color`</td>
            <td>
                <ul>
                    <li>`--wcs-select-icon-color-default`: default icon color</li>
                    <li>`--wcs-select-icon-color-focus`: icon color on focus</li>
                    <li>`--wcs-select-icon-color-disabled`: icon color when disabled</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>`--wcs-select-border-color`</td>
            <td>
                <ul>
                    <li>`--wcs-select-control-border-color-default`: default border color</li>
                    <li>`--wcs-select-control-border-color-focus`: border color on focus</li>
                    <li>`--wcs-select-control-border-color-disabled`: border color when disabled</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>`--wcs-select-outline-color`</td>
            <td>`--wcs-select-control-border-color-focus`</td>
        </tr>
        <tr>
            <td>`--wcs-select-option-height`</td>
            <td>`--wcs-select-option-height` from `wcs-select-option`</td>
        </tr>
        <tr>
            <td>`--wcs-select-border-width`</td>
            <td>
                <ul>
                    <li>`--wcs-select-overlay-border-width`: border width of the overlay which contains the options</li>
                    <li>`--wcs-select-control-border-width`: default border width</li>
                    <li>`--wcs-select-control-border-width-active`: border width when select is opened</li>
                    <li>`--wcs-select-control-border-width-focus`: border width when select is focused</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>`--wcs-select-border-radius`</td>
            <td>
                <ul>
                    <li>`--wcs-select-control-border-radius`: default border radius</li>
                    <li>`--wcs-select-overlay-border-radius`: border radius of the overlay which contains the options</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

#### wcs-native-select

> All existing `--wcs-select-native-*` variables have been renamed to `--wcs-native-select-*` to match the component name.

<table>
    <thead>
        <tr>
            <th>Deleted variable</th>
            <th>Replace with</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>`--wcs-select-native-outline-color`</td>
            <td>
                <ul>
                    <li>`--wcs-native-select-border-color-default`: default border color</li>
                    <li>`--wcs-native-select-border-color-focus`: border color on focus</li>
                    <li>`--wcs-native-select-border-color-disabled`: border color when disabled</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

#### wcs-switch

<table>
    <thead>
        <tr>
            <th>Deleted variable</th>
            <th>Replace with</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>`--wcs-switch-bullet-color-initial`</td>
            <td>`--wcs-switch-dot-color-initial`: for dot color inside switch when switch is not selected</td>
        </tr>
        <tr>
            <td>`--wcs-switch-bullet-color-final`</td>
            <td>`--wcs-switch-dot-color-final`: for dot color inside switch when switch is selected</td>
        </tr>
        <tr>
            <td>`--wcs-switch-text-color`</td>
            <td>
                <ul>
                    <li>`--wcs-switch-text-color-default`: text color default</li>
                    <li>`--wcs-switch-text-color-selected`: text color when switch is selected</li>
                    <li>`--wcs-switch-text-color-hover`: text color when switch is hovered</li>
                    <li>`--wcs-switch-text-color-disabled`: text color when switch is disabled</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

#### wcs-badge

<table>
    <thead>
        <tr>
            <th>Deleted variable</th>
            <th>Replace with</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                --wcs-badge-ligther-percentage
            </td>
            <td>
                You do not need this token anymore : the "lighter" badge now use another color palette, not opacity.
            </td>
        </tr>
        <tr>
            <td>
                These classes for the badge have been removed :
                - **`wcs-light`**
                - **`wcs-dark`**
            </td>
            <td>
                - Replace `class="wcs-light"` with `class="wcs-secondary"` and `color="lighter"`
                - Replace `class="wcs-dark"` with `class="wcs-secondary"` and `color="initial"`
            </td>
        </tr>
    </tbody>
</table>

#### wcs-tabs

<table>
    <thead>
    <tr>
        <th>Deleted variable</th>
        <th>Replace with</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>`--wcs-tabs-width`</td>
        <td>This variable was not used because `width` is always set to `auto` for the tabs.</td>
    </tr>
    <tr>
        <td>`--wcs-tabs-padding-horizontal`</td>
        <td>Use `--wcs-tabs-padding-left`/`--wcs-tabs-padding-right` instead.</td>
    </tr>
    <tr>
        <td>`--wcs-tabs-padding-vertical`</td>
        <td>Use `--wcs-tabs-padding-top`/`--wcs-tabs-padding-bottom` instead.</td>
    </tr>
    </tbody>
</table>

#### wcs-grid

<table>
    <thead>
    <tr>
        <th>Deleted variable</th>
        <th>Replace with</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>`--wcs-grid-header-sort-arrow-color`</td>
        <td>Use `--wcs-grid-header-sort-arrow-color-default` and `--wcs-grid-header-sort-arrow-color-hover` instead</td>
    </tr>
    </tbody>
</table>

#### wcs-header

With design tokens migration, the `wcs-header` **is no longer dark**. As you can put `wcs-button` inside the `wcs-header`, you have to verify the button is still visible. 

Indeed, on storybook, we applied the `.wcs-light` class to the button to make it visible. As the `wcs-header` is no longer dark, you have to remove this class from the button to keep it visible if you have used it.

**Before**

<Unstyled>
    <wcs-header style={{
        display: 'block',
        boxShadow: 'rgba(0, 0, 0, 0.14) 0px 4px 5px 0px, rgba(0, 0, 0, 0.12) 0px 1px 10px 0px, rgba(0, 0, 0, 0.2) 0px 2px 4px -1px',
        '--wcs-header-background-color': '#333',
        '--wcs-header-border-bottom': 'none',
    }}>
        <img
            style={{'--wcs-header-logo-height': '60px'}}
            slot="logo"
            alt="SNCF"
            src="sncf-logo.png"
        />
        <h1 style={{color: 'white'}} slot="title">Votre superbe application</h1>
        <div slot="actions">
            <wcs-button mode="clear" class="wcs-light">
                <span>Connexion</span>
                <wcs-mat-icon icon="person_outline"></wcs-mat-icon>
            </wcs-button>
        </div>
    </wcs-header>
</Unstyled>

```html
<wcs-header>
    <img
        slot="logo"
        alt="SNCF"
        src="data:image/svg+xml;base64,PD94...."
    />
    <h1 slot="title">Votre superbe application</h1>
    <div slot="actions">
        <wcs-button mode="clear" class="wcs-light"> <!--The class wcs-light was here-->
            <span>Connexion</span>
            <wcs-mat-icon icon="person_outline"></wcs-mat-icon>
        </wcs-button>
    </div>
</wcs-header>
```

**After**

<Unstyled>
    <wcs-header>
        <img
            style={{'--wcs-header-logo-height': '60px'}}
            slot="logo"
            alt="SNCF"
            src="sncf-logo.png"
        />
        <h1 slot="title">Votre superbe application</h1>
        <div slot="actions">
            <wcs-button mode="clear">
                <span>Connexion</span>
                <wcs-mat-icon icon="person_outline"></wcs-mat-icon>
            </wcs-button>
        </div>
    </wcs-header>
</Unstyled>

```html
<wcs-header>
    <img
        slot="logo"
        alt="SNCF"
        src="data:image/svg+xml;base64,PD94...."
    />
    <h1 slot="title">Votre superbe application</h1>
    <div slot="actions">
        <wcs-button mode="clear"> <!--The class is no longer applied-->
            <span>Connexion</span>
            <wcs-mat-icon icon="person_outline"></wcs-mat-icon>
        </wcs-button>
    </div>
</wcs-header>
```
